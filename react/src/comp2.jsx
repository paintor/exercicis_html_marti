import React from "react";
import "./css.css";

class Titulo extends React.Component{
    render(){
        //podem barrejar classname i inline
        return <h1 className="tita" style={{color:"red",fontSize:"100px"}}>{this.props.texto}</h1>; //elements html dins js amb {}
    }
}
export {Titulo};

class BolaX extends React.Component{
    render(){
        //sempre recordar que l argument de clases react es props, llavors es this.props.nom_prop
        let tamany = this.props.talla;
        return <div className="BolaX" style={{width:tamany ,height:tamany ,margin:this.props.margin, backgroundColor:this.props.fondo, lineHeight:tamany} } >BolaX Babe</div>;
    }
}
export{BolaX};
class CuadradoB extends React.Component{
    render(){
        //sempre recordar que l argument de clases react es props, llavors es this.props.nom_prop
        let tamany = this.props.talla;
        return <div className="CuadradoB" style={{width:tamany ,height:tamany ,margin:this.props.margen, borderColor: this.props.color, borderWidth: this.props.grosor}} ></div>;
    }
}
export{CuadradoB}

class Mosca extends React.Component{
    render(){
        let color = this.props.color;
        return <div className="bug icon fa fa-bug" style={{color: color, fontSize:"40px"}}/>
    }
}
export{Mosca}

class Capital extends React.Component{
    render(){
        return <p className="city">{this.props.nom[0]}<br className="capi"/>{this.props.nom}</p>
    }
}
export{Capital}

class Gato extends React.Component{
    render(){
        let url = `http://placekitten.com/${this.props.ancho}/${this.props.alto}`;
        return <figure className="gato" style={{textAlign:"center"}}>
            <img className="gato" src={url}></img>
            <figcaption>{this.props.nombre}</figcaption>
            </figure>
    }
}
export{Gato}

class BolaBingo extends React.Component{
    render(){
        return <div className="bola BolaBingo">{this.props.num}</div>
    }
}
export{BolaBingo}