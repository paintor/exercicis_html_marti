import React from "react";
import "./css.css";

//1
class Hello extends React.Component{
    render(){
        return <h1> Hello,{this.props.name}</h1>;
    }
}

export {Hello};

//2
class Bola extends React.Component{
    render(){
        return <div className="bola"></div>;
    }
}
export{Bola}

class Cuadrado extends React.Component{
    render(){
        return <div className="cuadrat"></div>;
    }
}

export{Cuadrado};

class Separador extends React.Component{
    render(){
        return <div className="Separador"><hr className="u"/><hr className="dos"/></div>;
    }
}

export{Separador};