import React from "react";
// import Hello from "./Hello";
import {Hello,Bola,Cuadrado,Separador} from "./comp1.jsx"
import {Titulo,BolaX,CuadradoB,Mosca,Capital,Gato,BolaBingo} from "./comp2.jsx"


// si nomes hi ha un component i esta exportat com a default no shan d usar els {}

export default () => (
  <>
    {/* <h1>Welcome to React Parcel Micro App!</h1>
    <p>Hard to get more minimal than this React app.</p> */}
    <Hello name=" Jove explorador"/>
    <Bola/>
    <br/>
    <Cuadrado/>
    <Separador/>
    <Titulo texto="hey, whatsupp --->>>>"/>
    <BolaX talla="200px" margin="10px" fondo="lightGreen"/>
    <CuadradoB talla="200px" margen="10px" grosor="25px" color="red" />
    <Mosca color="blue"/>
    <Capital nom="Barcelona" />
    <Gato ancho="200" alto="200" nombre="Garfield" />
    <BolaBingo num="22" />
    <h4>lets go to state now----></h4>

  </>
);
